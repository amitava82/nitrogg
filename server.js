var express = require('express');
var path = require('path');
var app = express();

app.use(express.static(__dirname + '/public'));


app.get("*", function(req, res){

    res.sendFile(path.resolve(__dirname, './public/index.html'));
});

app.listen(3100, function(){
    console.log('app listening on ', 3100);
});