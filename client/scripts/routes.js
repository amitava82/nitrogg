import React from 'react';
import {Route, IndexRoute, IndexRedirect, Redirect} from 'react-router';

import AccountContainer from './containers/account';
import HomeContainer from './containers/home';
import NotFound from './containers/misc/NotFound';

import App from './app';


export default (store) => {

    // function ensureLoggedIn(nextState, replace, cb) {
    //     const {session: {isLoggedIn, user}} = store.getState();
    //     if (!isLoggedIn && !ls.get('session')) {
    //         replace({pathname: '/login'});
    //     }
    //     cb();
    // }

    return (
        <Route path="/" component={App}>
            <IndexRoute component={HomeContainer} />
            <Route path="home" component={HomeContainer} />
            <Route path="account" component={AccountContainer} />
            <Route path="*" component={NotFound}/>
        </Route>
    );
};