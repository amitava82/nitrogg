import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import range from 'lodash/range';

import {loadBackpack} from '../../redux/modules/backpack';

@connect(state=>({backpack: state.backpack}), {loadBackpack})
export default class Backpack extends React.Component {

    componentWillMount(){
        this.props.loadBackpack();
    }

    render() {

        const items = this.props.backpack.items.map(i => (
            <div className={`weapon-block weapon-block-${i.color}`}>
                <div className="price-info">
                    <div className="info">Sold</div>
                    <div className="price">$21</div>
                </div><img src={`https://steamcommunity-a.akamaihd.net/economy/image/${i.image}`} />
                <div className="title">
                    <p>{i.name}</p>
                    <p>Oceanic</p>
                </div>
            </div>
        ));

        return (
            <div className="sidebar-right">
                <div className="header">
                    <p>My Backpack</p>
                </div>
                <div className="backpack-container">
                    {items}
                </div>
                <div className="footer">
                    <div className="value">
                        <p><span>Selected Value</span><span className="badge">$123.00</span></p>
                    </div>
                    <div className="button-group">
                        <button className="btn btn-success">SELL</button>
                        <button className="btn btn-primary">WITHDRAW</button>
                    </div>
                </div>
            </div>
        )
    }
}