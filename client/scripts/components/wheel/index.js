import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import noop from 'lodash/noop';
import range from 'lodash/range';

import {init, start, stop} from '../../redux/modules/wheel';

@connect(state => ({wheel: state.wheel}), {start, stop, init})
export default class Wheel extends React.Component {

    static defaultProps = {
        winning: 5,
        duration: 3,
        totalItems: 20,
        onWin: noop,
        start: noop
    };

    componentWillMount() {
        this.props.init();
    }


    render() {
        const {items, spinning, winningNumber} = this.props.wheel;
        
        const list = items.map(i => (
            <div key={i.id} className={`weapon-block weapon-block-${i.color}`}>
                <img src={`https://steamcommunity-a.akamaihd.net/economy/image/${i.image}`}/>
                <div className="title">
                    <p>{i.name}</p>
                    <p>Oceanic</p>
                </div>
            </div>
        ));
        
        return (
            <div className="weapons-container lucky-wheel">
                {list}
            </div>
        );
    }
}