import React from 'react';

export default class Footer extends React.Component {

    render() {
        return (
            <footer>
                <p>
                    <span>&copy; 2016 Nirto GG. All Rights Reserved. </span>
                    <a href="facebook.com" className="icon icon-facebook"></a>
                    <a href="facebook.com" className="icon icon-twitter"></a>
                </p>
            </footer>
        )
    }
}