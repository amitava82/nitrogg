import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import range from 'lodash/range';
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');

import {update} from '../../redux/modules/ticker';

@connect(state=>({
    ticker: state.ticker
}), {
    update
})
export default class Ticker extends React.Component {

    componentDidMount(){
        setInterval(this.props.update, 2000);
    }
    render() {
        const items = this.props.ticker.items.map((i) => (
            <div key={i.id} className={`weapon-block weapon-block-${i.color}`}><img src={`https://steamcommunity-a.akamaihd.net/economy/image/${i.image}`} />
                <p>{i.name}</p>
                <p>Oceanic</p>
            </div>
        ));

        return (
            <div className="sidebar-left">
                <ReactCSSTransitionGroup
                    transitionName="tick"
                    transitionEnterTimeout={300}
                    transitionLeaveTimeout={300}>
                    {items}
                </ReactCSSTransitionGroup>
                <div className="ticker">

                </div>
                <div className="case-stat">
                    <div>12121212</div>
                    <small>Cases Opened</small>
                </div>
            </div>
        )
    }
}