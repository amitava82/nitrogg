import React from 'react';
import autobind from 'autobind-decorator';
import {Link} from 'react-router';

export default class Header extends React.Component {

    render() {
        return (
            <nav className="navbar header flex center">
                <ul className="items flex-item">
                    <li className="menu">
                        <Link to="/">NITRO GG</Link>
                    </li>
                </ul>
                <ul className="items">
                    <li className="menu flex center">
                        <div className="balance">
                            <span className="amount">$123</span>
                            <span className="buy-btn" onClick={this.props.toogleBuyModal}>
                                <i className="icon icon-plus"></i>
                            </span>
                        </div>
                    </li>
                    <li className="menu">
                        <Link className="account" to="/account">
                            <span>Mike123</span>
                            <img src="/images/avatar.png" />
                        </Link>
                    </li>
                </ul>
            </nav>
        )
    }
}