export const API_ERROR_TYPE = "API_ERROR";
export const RESET_API_ERROR = "RESET_API_ERROR";

export const SITE_TITLE = "NitroGG";
export const SITE_DESCRIPTION = "Go go go!";
export const KEYWORDS = "";

export const COLORS = ['blue', 'green', 'red', 'purple', 'gold'];