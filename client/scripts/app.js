import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import Toastr from './utils/toastr';
import {storeSession} from './redux/modules/session';

import Header from './components/header';
import Footer from './components/footer';
import Ticker from './components/ticker';
import Backpack from './components/backpack';
import PurchaseModal from './modals/PurchaseModal';

import {toogleBuyModal} from './redux/modules/ui';

import '../scss/export.scss';

@connect(state => ({session: state.session, ui: state.ui}), {toogleBuyModal})
export default class App extends React.Component {

    render() {

        const {session: {isLoggedIn}, toogleBuyModal} = this.props;

        return (
            <div id="main">
                <Header toogleBuyModal={toogleBuyModal} />
                <Ticker />
                <div className="container">
                    {this.props.children}
                </div>
                <Backpack />
                <Footer />
                <Toastr />
                <PurchaseModal />
            </div>
        );
    }
}