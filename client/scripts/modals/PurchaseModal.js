import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import Modal from 'react-modal';

import {toogleBuyModal} from '../redux/modules/ui';

@connect(state=>({ui: state.ui}))
export default class PurchaseModal extends React.Component {

    @autobind
    close(){
        this.props.dispatch(toogleBuyModal());
    }

    render() {

        const styles = {
            overlay: {
                backgroundColor   : 'rgba(0, 0, 0, 0.75)',
                zIndex: 9999
            }
        };

        return (
            <Modal
                style={styles}
                className="purchase-modal"
                isOpen={this.props.ui.buying}
                onRequestClose={this.close}>
                <h1 className="mb-l">Filling up the balance</h1>
                <div className="modal-content">
                    <div className="input-addon mb-m">
                        <input className="form-control" type="text" placeholder="Enter the amount" autofocus/>
                        <span className="addon">$</span>
                    </div>
                    <button className="btn lg btn-success">Pay now</button>
                </div>
            </Modal>
        )
    }
}