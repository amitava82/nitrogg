/*
Middleware that passes api client to promise middleware
 */
export default function clientMiddleware(client) {
    return ({dispatch, getState}) => {

        return next => action => {
            const {payload, type, ...rest } = action;
            const {session: {token, isLoggedIn}} = getState();

            if(payload && typeof payload.promise === 'function'){
                action.payload.promise = payload.promise(client);
            }

            return next(action);
        };
    };
}