import {LOCATION_CHANGE} from 'react-router-redux';
import createAction from '../createActions';

const [TOGGLE_BUY] = createAction('ui', ["TOGGLE_BUY"]);

const initialState = {
    buying: false
};

export default function (state = initialState, action = {}) {
    const {type, payload} = action;
    
    switch (type){
        case TOGGLE_BUY:
            return {...state, buying: !state.buying};
        default:
            return state
    }
}

export function toogleBuyModal() {
    return {
        type: TOGGLE_BUY
    }
}