/**
 * Created by amitava on 14/08/16.
 */
import createAction from '../createActions';
import {COLORS} from '../../constants';
import sample from 'lodash/sample';
import random from 'lodash/random';
import {IMAGES, NAMES} from '../../../data/drops';

const [UPDATE] = createAction('ticker', ['UPDATE']);

const initialState = {
  items: []  
};

export default function (state = initialState, action= {}) {
    const {payload, type} = action;
    
    switch (type){
        case UPDATE:
            const items = [...state.items];

            items.unshift(payload);
            if(items.length > 20){

                items.splice(-1);
            }
            return {...state, items};
        
        default:
            return state;
    }
}

export function update() {
    return {
        type: UPDATE,
        payload: {
            color: sample(COLORS),
            id: random(0, 9999),
            name: sample(NAMES),
            image: sample(IMAGES)
        }
    }
}