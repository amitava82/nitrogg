import { resolve, reject as _reject } from '../middleware/simple-promise';
import extend from 'lodash/extend';
import Promise from 'bluebird';

import createAction from '../createActions';

const [STORE_SESSION, LOGIN, LOGOUT, SIGNUP, SET_PASSWORD, UPDATE_PROFILE] =
    createAction('session', ["STORE_SESSION", "LOGIN", "LOGOUT", "SIGNUP", "SET_PASSWORD", "UPDATE_PROFILE"]);

const initialState = {
    user: null,
    isLoggedIn: false,
    loading: false,
    error: null
};

export default function(state = initialState, action = {}){
    switch (action.type){
        case STORE_SESSION:
            return extend({}, state, {
                user: action.payload,
                isLoggedIn: true,
                loading: false,
                error: null
            });

        case LOGIN:
            return extend({}, state, {
                loading: true,
                user: null,
                token: null,
                isLoggedIn: false,
                error: null
            });

        case UPDATE_PROFILE:
            return extend({}, state, {
                error: null,
                loading: true
            });

        case _reject(LOGIN):
        case _reject(UPDATE_PROFILE):
            return extend({}, state, {
                loading: false,
                error: action.payload
            });

        case resolve(LOGIN):
            return extend({}, state, {
                loading: false,
                user: action.payload,
                error: null,
                isLoggedIn: true
            });

        case resolve(UPDATE_PROFILE):
            return extend({}, state, {
                user: action.payload,
                error: null,
                loading: false
            });
        
        case LOGOUT:
            return extend({}, state, initialState);

        default:
            return state;
    }
}

export function storeSession(session){
    return {
        type: STORE_SESSION,
        payload: session
    }
}

export function doLogin(data) {
    return {
        type: LOGIN,
        payload: {
            promise: (api) => api.post('/auth/login', {
                data,
                prefix: false
            })
        }
    }
}

export function signup(data) {
    return {
        type: SIGNUP,
        payload: {
            promise: api => api.post('/auth/signup', {
                data,
                prefix: false
            })
        }
    }
}

export function logout(){
    return {
        type: LOGOUT
    }
}

export function requestResetPassword(username) {
    return {
        type: "REQUEST_RESET",
        payload: {
            promise: api => api.post(`/auth/password/forgot`, {data: {username}, prefix: false})
        }
    }
}

export function resetPassword(password, code) {
    return {
        type: SET_PASSWORD,
        payload: {
            promise: api => api.post(`/auth/password/reset`, {
                data: {
                    code,
                    password
                },
                prefix: false
            })
        }
    }
}

export function updateProfile(profile) {
    return {
        type: UPDATE_PROFILE,
        payload: {
            promise: api => api.post('users/profile', {
                data: profile
            })
        }
    }
}