/**
 * Created by amitava on 14/08/16.
 */
import createAction from '../createActions';
import {COLORS} from '../../constants';
import sample from 'lodash/sample';
import random from 'lodash/random';
import range from 'lodash/range';
import {IMAGES, NAMES} from '../../../data/drops';

const [LOAD] = createAction('backpack', ['LOAD']);

const initialState = {
  items: []  
};

export default function (state = initialState, action= {}) {
    const {payload, type} = action;
    
    switch (type){
        case LOAD:
            return {...state, items: payload};
        
        default:
            return state;
    }
}

export function loadBackpack() {
    return {
        type: LOAD,
        payload: range(0, 23).map(i => ({
            color: sample(COLORS),
            id: random(0, 9999),
            name: sample(NAMES),
            image: sample(IMAGES)
        }))
    }
}