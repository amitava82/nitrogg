/**
 * Created by amitava on 14/08/16.
 */
import createAction from '../createActions';
import {COLORS} from '../../constants';
import sample from 'lodash/sample';
import random from 'lodash/random';
import range from 'lodash/range';
import {IMAGES, NAMES} from '../../../data/drops';

const [INIT, START, STOP] = createAction('wheel', ['INIT', 'START', 'STOP']);

const initialState = {
  items: [],
  spinning: false,
  winningNumber: 0  
};

export default function (state = initialState, action= {}) {
    const {payload, type} = action;
    
    switch (type){
        case INIT:
            return {...state, items: payload, spinning: false, winningNumber: random(0, 20)};
        
        case START:
            return {...state, spinning: true};
        
        case STOP:
            return {...state, spinning: false};
        
        default:
            return state;
        
    }
}

export function init() {
    return {
        type: INIT,
        payload: range(0, 23).map(i => ({
            color: sample(COLORS),
            id: random(0, 9999),
            name: sample(NAMES),
            image: sample(IMAGES)
        }))
    }
}

export function start() {

    return dispatch => {
        setTimeout(_ => {
            dispatch({
                type: STOP
            })
        }, random(3000, 6000));

        return dispatch({
            type: START
        });
    }

}

export function stop() {
    return {
        type: STOP
    }
}