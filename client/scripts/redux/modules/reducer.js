import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import errorMessage from './error';
import toast from './toast';
import session from './session';
import ui from './ui';
import ticker from './ticker';
import backpack from './backpack';
import inventory from './inventory';
import wheel from './wheel';

export default combineReducers({
    errorMessage,
    toast,
    session,
    ui,
    ticker,
    backpack,
    inventory,
    wheel,
    routing: routerReducer
});