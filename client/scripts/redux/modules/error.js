import {API_ERROR_TYPE, RESET_API_ERROR} from '../../constants';
import get from 'lodash/get';

export default function errorMessage(state = null, action) {
    const { type, payload } = action;

    if (type === RESET_API_ERROR) {
        return null
    } else if (type === API_ERROR_TYPE) {
        return get(payload, 'message', payload);
    }

    return state
}