const app = 'nitro';

export default function (module, constants) {
       return constants.map(i => {
           return `${app}/${module}/${i}`;
       });
}