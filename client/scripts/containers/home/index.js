import React from 'react';
import {connect} from 'react-redux';
import classNames from 'classnames';
import autobind from 'autobind-decorator';
import range from 'lodash/range';

import Wheel from '../../components/wheel';
import {start} from '../../redux/modules/wheel';

@connect(state => ({wheel: state.wheel}), {start})
export default class HomeContainer extends React.Component {

    render() {
        const spinning = this.props.wheel.spinning;

        const boxes = range(0,20).map(i => (
            <div className="nitro-box">
                <div className="image"><img src="images/nitro_box.png"/></div>
                <div className="details">
                    <p>Milspec</p>
                    <p>$0.30</p>
                </div>
            </div>
        ));

        const btnClass = classNames('btn btn-success', {loading: spinning});
        
        return (
            <div className="home-container">
                <div className="hero">
                    <div className="nitro-box-preview">
                        <div><img src="images/nitro_box.png"/></div>
                        <button disabled={spinning} onClick={this.props.start} className={btnClass}>Open Case $0.30</button>
                    </div>
                    <div className="weapons-container-wrapper">
                        <Wheel />
                    </div>
                </div>
                <div className="nitro-box-container">
                    <div className="title">
                        <p>Select a Case</p>
                    </div>
                    <div className="nitro-box-list">
                        {boxes}
                    </div>
                </div>
            </div>
        )
    }
}