import React from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';
import range from 'lodash/range';

import {loadInventory} from '../../redux/modules/inventory';

@connect(state => ({inventory: state.inventory}), {loadInventory})
export default class AccountContainer extends React.Component {

    componentWillMount(){
        this.props.loadInventory();
    }

    render() {
        const {inventory} = this.props;
        const ln = inventory.items.length;
        const itemsList = inventory.items.map( i=>  (
                <div className={`weapon-block weapon-block-${i.color}`}>
                    <div className="price-info">
                        <div className="info">Sold</div>
                        <div className="price">$21</div>
                    </div>
                    <img src={`https://steamcommunity-a.akamaihd.net/economy/image/${i.image}`} />
                    <div className="title">
                        <p>{i.name}</p>
                        <p>Oceanic</p>
                    </div>
                </div>
            )
        );

        return (
            <div className="account-container">
                <div className="weapons-container-wrapper">
                    <div className="header">
                        <div className="profile">
                            <div className="image"><img src="images/avatar.png"/></div>
                            <div className="details">
                                <h5>Name</h5>
                                <p>Last Online: 25 June 2009</p>
                                <div className="cash-info">
                                    <span className="cash">$0</span>
                                    <span className="add">
                                        <i className="fa fa-plus"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="trade-url flex-item">
                            <p>Your Trade URL</p>
                            <input className="form-control" type="url" placeholder="http://www.your-domain.com"/>
                            <button className="btn lg btn-success">Save</button>
                        </div>
                    </div>
                    <div className="weapons-container">
                        {itemsList}
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                        <div className="weapon-block placeholder"></div>
                    </div>
                </div>
            </div>
        )
    }
}